import React from 'react';

const Money: React.FC<any> = (props) => {

    let splitValue = String(props.value).split('.')
    let intValue = splitValue[0]
    let modValue = splitValue[1] && Number(splitValue[1])

    const makeMoneyCode = (code: any): any => {
        switch (code) {
            case 'RUB':
                return '₽'
            case 'EUR':
                return '€'
            case 'GBP':
                return '£'
            case 'USD':
                return '$'
        }
    }

    if (props.type !== "external") {
        return (
            <span>
                <span>{intValue}</span>
                {modValue && <span>,{modValue}</span>}
                {props.currency && <span>{makeMoneyCode(props.currency)}</span>}
            </span>
        )
    } else {
        return (<span></span>)
    }
};

export default Money;
