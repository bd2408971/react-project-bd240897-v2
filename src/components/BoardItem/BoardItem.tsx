import React from 'react';
import cn from 'classnames';
import Money from '../Money/Money';

import styles from './BoardItem.module.css';

const BoardItem: React.FC<any> = ({type, title, amount, currency, customTitle}) => {

    let newTitle;
    if (customTitle) {
        newTitle = customTitle
    } else {
        newTitle = title
    }
    return (
        <div className={styles.item}>
            <div className={cn(styles.logo, styles[`logo_${type}`])}/>
            <div className={styles.title}>{newTitle}</div>

            {type !== "external" && <Money value={amount} currency={currency} type={type}/>}
        </div>
    );
};

export default BoardItem;
