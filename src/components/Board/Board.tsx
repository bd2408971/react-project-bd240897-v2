import React from 'react';

import BoardItem from '../BoardItem/BoardItem';

import styles from './Board.module.css';

const Board: React.FC<any> = ({accounts}) => {
    let copyAccounts = [...accounts]
    return (
        <div className={styles.board}>
            {copyAccounts.sort(compFun).map(el =>
                <BoardItem
                    key={el.id}
                    type={el.type}
                    title={el.title}
                    amount={el.amount}
                    currency={el.currency}
                    customTitle={el.customTitle}
                    id={el.id}
                />)}
        </div>
    )
}

export default Board;


let mapCardType = {
    debit: 5,
    credit: 4,
    external: 3,
    saving: 2,
    loan: 1
}

let mapCardWallet = {
    RUB: 5,
    USD: 4,
    GBP: 3,
}

const compFun = (a: any, b: any): any => {

    if (mapCardType[a.type] < mapCardType[b.type]) {
        return +1
    } else if (mapCardType[a.type] > mapCardType[b.type]) {
        return -1
    } else {
        if (mapCardWallet[a.currency] < mapCardWallet[b.currency]) {
            return +1
        } else if (mapCardWallet[a.currency] > mapCardWallet[b.currency]) {
            return -1
        } else {
            return 0
        }
    }
}
