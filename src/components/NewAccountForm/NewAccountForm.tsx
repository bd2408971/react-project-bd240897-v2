import React from 'react';
import MaskedInput from 'react-maskedinput';

import Button from '../Button/Button';

import styles from './NewAccountForm.module.css';

export default class NewAccountForm extends React.Component<any, any> {
    constructor(props) {
        super(props);

        this.state = {
            cardNumber: '',
            month: '',
            year: '',
        };
    }

    handleSubmit = (event: any): any => {
        event.preventDefault()

        if (this.state.cardNumber && this.state.month && this.state.year) {
            let savedCardNumber = `Привязанная карта *${this.state.cardNumber.split(" ")[3]}`

            let newObj = {
                id: Date.now(),
                type: 'external',
                title: savedCardNumber,
            }

            this.props.handleSubmit(newObj)
            this.setState({
                cardNumber: '',
                month: '',
                year: '',
            })
        }
    };

    handleInputChange = (event: any): any => {
        this.setState({
            [event.target.name]: event.target.value,
        });
    };

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <h2>Привязка банковской карты</h2>
                <div className={styles.cardForm}>
                    <MaskedInput
                        mask="1111 1111 1111 1111"
                        name="cardNumber"
                        value={this.state.cardNumber}
                        onChange={this.handleInputChange}
                        placeholder="Номер карты"
                        className={styles.input}
                    />
                    <div className={styles.validThruFieldset}>
                        <label className={styles.label}>VALID THRU</label>
                        <input name="month"
                               value={this.state.month}
                               onChange={this.handleInputChange}
                               className={`${styles.input} ${styles.inputDate}`}
                        />
                        /
                        <input
                            name="year"
                            value={this.state.year}
                            onChange={this.handleInputChange}
                            className={`${styles.input} ${styles.inputDate}`}
                        />
                    </div>
                    <Button type="submit">Привязать</Button>
                </div>
            </form>
        );
    }
}
